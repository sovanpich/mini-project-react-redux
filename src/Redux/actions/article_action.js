import {fetchArticle} from '../../services/article_service'
import {articleAction} from './articleActionType'

export const onFetchArticle = () => async dp => {
    const article = await fetchArticle()
    dp({
        type: articleAction.GET_Article,
        payload: article
    })
}