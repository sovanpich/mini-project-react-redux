import { articleAction } from '../actions/articleActionType';
const initialState = {
    articles:[]
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

    case articleAction.GET_Article:
        return { ...state, articles:[...payload] }

    default:
        return state
    }
}
