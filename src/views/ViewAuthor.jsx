import React, { useState, useEffect } from 'react'
import { Container, Table,Button } from 'react-bootstrap'
import { fetchAuthor} from '../services/author_service';

function ViewAuthor() {
    const [authors, setAuthors] = useState([])
    const fetchAuthorData = async ()=>{
        let authorData = await fetchAuthor();
        setAuthors(authorData);
    }
    useEffect(() => {
       fetchAuthorData()
    }, [])

    return (
        <Container>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        authors.map((author) => {
                            <tr key={author._id}>
                                <td>{author._id}</td>
                                <td>{author.name}</td>
                                <td>{author.email}</td>
                                <td>{author.image}</td>
                                <td>
                                    <Button variant="outline-warning" className="my-2">Edit</Button>
                                    <Button variant="outline-danger">View</Button>
                                </td>
                            </tr>
                        })
                    }


                </tbody>
            </Table>
        </Container>
    )
}

export default ViewAuthor
