import React, { useEffect, useState } from "react";
import { Button, Col, Container, Row, Card } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from 'redux';
import { onFetchArticle } from '../Redux/actions/article_action';
function Article() {

  const dispatch = useDispatch()
  const articles = useSelector((state) => state.article_reducer.articles);
  const onFetch = bindActionCreators(onFetchArticle, dispatch)
  useEffect(() => {
    onFetch()
    console.log(onFetch())
  }, [])
  let articleCard = articles.map((article) => (

    <Col xl={3} key={article._id}>
      <Card className="my-2">
        <Card.Img
          variant="top"
          style={{ objectFit: "cover", height: "150px" }}
          src={article.image ? article.image : "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"}
        />
        <Card.Body>
          <Card.Title>{article.title}</Card.Title>
          <Card.Text className="text-line-3">
            {article.description}
          </Card.Text>
          <Button
            size="sm"
            variant="primary"
          // onClick={()=>
          //   history.push('/article/'+article._id)
          // }
          >
            View
          </Button>{" "}
          <Button
            size="sm"
            variant="warning"
          // onClick={()=>{
          //   history.push('/update/article/'+article._id)
          // }}
          >
            Edit
          </Button>{" "}
          {/* onClick={()=>onDelete(article._id)} */}
          <Button size="sm" variant="danger" >
            Delete
          </Button>
        </Card.Body>
      </Card>
    </Col>
  ))
  return (
    <Container>
      <h1 className="text-center my-4">AMS Management</h1>

      <Row>
        <Col xl={3}>
          <Card className="my-2">
            <Card.Img
              variant="top"
              style={{ objectFit: "cover", height: "200px" }}
              src="https://images.unsplash.com/photo-1511367461989-f85a21fda167?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cHJvZmlsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&w=1000&q=80"
            />
            <Card.Body>
              <Card.Text className="text-line-3">
                Please Login
              </Card.Text>
              <Button
                size="sm"
                variant="primary"

              >
                Login With Google
              </Button>
            </Card.Body>
          </Card>
        </Col>
        <Col>
          <Row>
            {articleCard}
          </Row>
        </Col>
      </Row>

    </Container>
  );
}

export default Article;
