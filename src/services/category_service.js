import api from "../api/api"


export const fetchCategory = async () => {

    let response = await api.get('category')

    return response.data.data

}
export const addCategory = async (category) => {
    let response = await api.post('category',category)
    return response.data.message
}