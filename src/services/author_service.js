import api from "../api/api"

export const fetchAuthor = async () => {
    let response = await api.get('author')
    return response.data.data
}
export const postAuthor = async (author) => {
    let response = await api.post('author', author)
    return response.data.message
}
export const fetchAuthorById = async (id) => {
    let response = await api.get('author/' + id)
    return response.data.data
}
export const uploadImage = async (file) => {

    let formData = new FormData()
    formData.append('image', file)

    let response = await api.post('images', formData)
    return response.data.url
}
export const updateAuthorById = async (id, newAuthor) => {
    let response = await api.patch('author/' + id, newAuthor)
    return response.data.message
}
